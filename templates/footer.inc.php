<!-- Main Footer -->
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="AdminLTE/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="AdminLTE/dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="AdminLTE/plugins/chart.js/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="AdminLTE/dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="AdminLTE/dist/js/pages/dashboard3.js"></script>

<!-- SweetAlert2 -->
<script src="AdminLTE/plugins/sweetalert2/sweetalert2.all.min.js"></script>

<!-- SweetAlert2 -->
<script src="AdminLTE/plugins/datatables/datatables.min.js"></script>
<!-- Modal dialog -->
<script src="AdminLTE/assets/js/page/modal_dialog.js"></script>

<!-- validate -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>

<!-- Main -->
<script src="AdminLTE/assets/js/page/main.js"></script>

