<?php

try {
    if (!isset($_SESSION['employee']) || !is_a($_SESSION['employee'], "Employee")) {
        header("Location: " . Router::getSourcePath() . "index.php");

    }

    ob_start();
    ?>
<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
    </ul>
</nav>
<!-- /.navbar -->

<div class=" content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">แดชบอร์ด</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">หน้าหลัก</a></li>
                        <li class="breadcrumb-item active">แดชบอร์ด</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
</div>
<!-- /.content-wrapper -->

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a class="brand-link">
        <img src="AdminLTE/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">S Super Cable</span>
    </a>

    <!-- Sidebar -->
<div class="sidebar">
  <!-- Sidebar user panel (optional) -->
  
  <?php include("templates/sidebar_profile.inc.php"); ?>


  <!-- Sidebar Menu -->
  <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
              <a  class="nav-link active">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>
                      หน้าหลัก
                      <i class="right fas fa-angle-left"></i>
                  </p>
              </a>
              <ul class="nav nav-treeview">
                  <li class="nav-item">
                      <a href="index.php?controller=Homepage&action=index" class="nav-link">
                          <i class="nav-icon fas fa-tachometer-alt"></i>
                          <p>
                              หน้าหลัก
                          </p>
                      </a>
                  </li>
              </ul>
          <li class="nav-item menu-open">
              <a  class="nav-link active">
                  <i class="nav-icon  fas fa-gift"></i>
                  <p>
                      เบิก/คืน
                      <i class="right fas fa-angle-left"></i>
                  </p>
              </a>
              <ul class="nav nav-treeview">
                  <li class="nav-item">
                      <a href="pages/promotion.html" class="nav-link">
                          <i class="nav-icon fas fa-gift "></i>
                          <p>
                              ส่งเสริมการขาย
                          </p>
                      </a>
                  </li>
              </ul>
         
          <li class="nav-item menu-open">
              <a  class="nav-link active">
                  <i class="nav-icon fas fa-sign-out-alt"></i>
                  <p>
                      ออกจากระบบ
                      <i class="right fas fa-angle-left"></i>
                  </p>
              </a>
              <ul class="nav nav-treeview">
                  <li class="nav-item">
                      <a href="#" onclick="logout()" class="nav-link">
                          <i class="nav-icon fas fa-sign-out-alt"></i>
                          <p>ออกจากระบบ </p>
                      </a>
                  </li>
              </ul>
      </ul>
  </nav>
  <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>


<?php 
    # modal dialog ( edit profile )
    include Router::getSourcePath()."views/modal/modal_editprofile.inc.php";
?>

<footer class="main-footer">
    <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 3.1.0-rc
    </div>
</footer>
    <?php
    $content = ob_get_clean();

    include Router::getSourcePath()."templates/layout.php";
} catch (Throwable $e) { // PHP 7++
    echo "Access denied: No Permission to view this page";
    exit(1);
}
?>